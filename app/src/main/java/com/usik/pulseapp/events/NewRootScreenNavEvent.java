package com.usik.pulseapp.events;


public class NewRootScreenNavEvent extends BaseNavEvent{
    public NewRootScreenNavEvent(String screen, Object data) {
        super(screen, data);
    }

    public NewRootScreenNavEvent(String screen) {
        super(screen);
    }
}
