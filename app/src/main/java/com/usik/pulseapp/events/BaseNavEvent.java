package com.usik.pulseapp.events;


public abstract class BaseNavEvent {
    private String screen;
    private Object data;

    public BaseNavEvent(String screen, Object data) {
        this.screen = screen;
        this.data = data;
    }

    public BaseNavEvent(String screen) {
        this.screen = screen;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
