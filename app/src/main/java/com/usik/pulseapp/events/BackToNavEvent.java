package com.usik.pulseapp.events;


public class BackToNavEvent extends BaseNavEvent {
    public BackToNavEvent(String screen, Object data) {
        super(screen, data);
    }

    public BackToNavEvent(String screen) {
        super(screen);
    }
}
