package com.usik.pulseapp.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class NetworkStateEvent {
    private boolean isNetworkAvailable;
}
