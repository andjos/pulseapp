package com.usik.pulseapp.events;


public class ReplaceNavEvent extends BaseNavEvent{
    public ReplaceNavEvent(String screen, Object data) {
        super(screen, data);
    }

    public ReplaceNavEvent(String screen) {
        super(screen);
    }
}
