package com.usik.pulseapp.events;

public class NavigateToNavEvent extends BaseNavEvent {

    public NavigateToNavEvent(String screen, Object data) {
        super(screen, data);
    }

    public NavigateToNavEvent(String screen) {
        super(screen);
    }
}
