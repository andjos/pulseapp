package com.usik.pulseapp.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.usik.pulseapp.di.qualifiers.ApplicationContext;

import javax.inject.Inject;


public class PreferenceManager {

    private static final String PREFERENCES_NAME_KEY = "pulse_prefs";
    private SharedPreferences mSharedPreferences;

    @Inject
    public PreferenceManager(@ApplicationContext Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFERENCES_NAME_KEY, 0);
    }

    public void clearData() {
        mSharedPreferences.edit().clear().apply();
    }

    public boolean isLogin() {
       return mSharedPreferences.getBoolean("isLogin", false);
    }

    public void login() {
        mSharedPreferences.edit().putBoolean("isLogin", true).apply();
    }

    public void logout() {
        mSharedPreferences.edit().putBoolean("isLogin", false).apply();
    }
}
