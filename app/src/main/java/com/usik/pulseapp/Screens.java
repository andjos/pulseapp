package com.usik.pulseapp;

/**
 * Created: Zorin A.
 * Date: 21.11.2016.
 */

public class Screens {
    public static final String MAIN_FRAGMENT_SCREEN = "main_fragment_screen";
    public static final String TEACHER_FRAGMENT_SCREEN = "teacher_fragment_screen";
    public static final String STUDENT_FRAGMENT_SCREEN = "student_fragment_screen";
    public static final String CONTACTS_FRAGMENT_SCREEN = "contacts_fragment_screen";
    public static final String ABOUT_FRAGMENT_SCREEN = "about_fragment_screen";
    public static final String LOGIN_FRAGMENT_SCREEN = "login_fragment_screen";
    public static final String REGISTER_FRAGMENT_SCREEN = "register_fragment_screen";
    public static final String FORGOT_PASS_FRAGMENT_SCREEN = "forgot_pass_fragment_screen";
    public static final String RESULTS_FRAGMENT_SCREEN = "results_fragment_screen";
    public static final String CREATE_POLL_FRAGMENT_SCREEN = "create_poll_fragment_screen";
    public static final String ANALYS_FRAGMENT_SCREEN = "analys_fragment_screen";
    public static final String STUDENT_POLL_FRAGMENT_SCREEN = "student_poll_fragment_screen";

}
