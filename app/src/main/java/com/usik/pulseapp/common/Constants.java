package com.usik.pulseapp.common;


public class Constants {
    public static final int DISK_CACHE_SIZE = 10 * 1024 * 1024;
    public static final String BASE_URL = "http://somebaseurl";

    public static final int SPLASH_DURATION = 3000;

}

