package com.usik.pulseapp.common;

import android.text.TextUtils;

/**
 * Created by Andrew on 12.04.2017.
 */

public class Utils {

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
