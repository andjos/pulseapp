package com.usik.pulseapp.lists.item_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.usik.pulseapp.R;
import com.usik.pulseapp.interfaces.AdapterClickListener;
import com.usik.pulseapp.mvp.models.QuestionModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class QuestionItemView extends LinearLayout implements ViewModel<QuestionModel> {
    QuestionModel data;

    @BindView(R.id.item_question_text)
    TextView title;
    @BindView(R.id.item_spinner)
    Spinner spinner;

    AdapterClickListener<QuestionModel> clickListener;
    List<Integer> markList;

    public QuestionItemView(Context context) {
        super(context);
        init(context);
    }

    public QuestionItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.item, this);
        ButterKnife.bind(this);
        markList = new ArrayList<>();
        for (int i = 1; i < 11; i++) {
            markList.add(i);
        }
    }

    @Override
    public QuestionModel getData() {
        return null;
    }

    @Override
    public void setData(QuestionModel data) {
        this.data = data;
        title.setText(data.getPollName());
        // Creating adapter for spinner
        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this.getContext(), android.R.layout.simple_spinner_item, markList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);


      spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            int mark = Integer.parseInt(parent.getItemAtPosition(position).toString());
            data.setMark(mark);
            clickListener.onItemClick(position, data);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    });
    }

    public void setClickListener(AdapterClickListener<QuestionModel> clickListener) {
        this.clickListener = clickListener;
    }
}
