package com.usik.pulseapp.lists;

import android.support.v7.widget.RecyclerView;
import android.view.View;


public class BaseViewHolder<T extends View> extends RecyclerView.ViewHolder {
    private T itemView;

    public BaseViewHolder(T itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public T getView() {
        return itemView;
    }

    public void setView(T view) {
        this.itemView = view;
    }
}
