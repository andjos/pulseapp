package com.usik.pulseapp.lists;

import android.view.ViewGroup;

import com.usik.pulseapp.interfaces.AdapterClickListener;
import com.usik.pulseapp.lists.item_view.QuestionItemView;
import com.usik.pulseapp.mvp.models.QuestionModel;

import javax.inject.Inject;


public class QuestionsAdapter extends BaseAdapter<QuestionModel, QuestionItemView, BaseViewHolder<QuestionItemView>> {

    AdapterClickListener<QuestionModel> clickListener;

    @Inject
    public QuestionsAdapter() {
    }

    public void setClickListener(AdapterClickListener<QuestionModel> clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public BaseViewHolder<QuestionItemView> onCreateViewHolder(ViewGroup parent, int viewType) {
        QuestionItemView view = new QuestionItemView(parent.getContext());
        view.setClickListener(clickListener);
        return new BaseViewHolder<>(view);
    }
}
