package com.usik.pulseapp.lists.item_view;


public interface ViewModel<T> {
    T getData();

    void setData(T data);
}
