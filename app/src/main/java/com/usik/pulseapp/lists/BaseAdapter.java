package com.usik.pulseapp.lists;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;


import com.usik.pulseapp.lists.item_view.ViewModel;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<M, V extends View & ViewModel,
        VH extends BaseViewHolder<V>>
        extends RecyclerView.Adapter<VH> {

    protected List<M> content = new ArrayList<>();

    @Override
    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(VH holder, int position) {
        V view = holder.getView();
        view.setData(content.get(position));
    }

    @Override
    public int getItemCount() {
        return (content == null) ? 0 : content.size();
    }

    public M getModel(int position) {
        return content.size() - 1 >= position ? content.get(position) : null;
    }

    public void addData(M data) {
        content.add(data);
        notifyItemInserted(content.size() - 1);
    }

    public List<M> getData() {
        return content;
    }

    public void setData(List<M> data) {
        if (data != null) {
            content = data;
            notifyDataSetChanged();
        }
    }

    public M getData(int position) {
        return content.get(position);
    }

    public void insertDataToStart(M data) {
        content.add(0, data);
        notifyItemInserted(0);
    }

    public void addData(List<M> data) {
        content.addAll(data);
        notifyItemRangeInserted(content.size() - 1 - data.size(), data.size());
    }

    public void removeData(int position) {
        if (getItemCount() > position) {
            content.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, content.size() + 1);
        }
    }

    public void updateData(int position, M data) {
        content.set(position, data);
        notifyItemChanged(position);

    }

    public void clearData() {
        content.clear();
        notifyDataSetChanged();
    }
}
