package com.usik.pulseapp.interfaces;

import android.support.v7.widget.Toolbar;

/**
 * Created by usik.a on 15.02.2017.
 */

public interface ToolbarActions {

    void showDrawerToggleButton();

    void showDrawerToggleButton(Toolbar toolbar);

    Toolbar getToolBar();

    void setSupportToolBar(Toolbar toolbar);

    void showBackButton();
}
