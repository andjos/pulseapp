package com.usik.pulseapp.interfaces;


public interface AdapterClickListener<T> {
    void onItemClick(int position, T data);
}
