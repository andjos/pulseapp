package com.usik.pulseapp.ui.fragment;

import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.mvp.presenter.CreatePollPresenter;
import com.usik.pulseapp.mvp.view.CreatePollView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Andrew on 12.04.2017.
 */

@FragmentWithArgs
public class CreatePollFragment extends BaseFragment
        implements CreatePollView {

    @InjectPresenter
    CreatePollPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_poll;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.create_new_poll_titile));
    }

    @OnClick(R.id.tv_save)
    public void onClick() {
        showToastMessage(R.string.poll_created);
    }
}
