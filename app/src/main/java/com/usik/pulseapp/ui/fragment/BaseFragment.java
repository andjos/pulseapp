package com.usik.pulseapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.usik.pulseapp.di.components.ActivityComponent;
import com.usik.pulseapp.di.components.FragmentComponent;
import com.usik.pulseapp.di.modules.FragmentModule;
import com.usik.pulseapp.interfaces.ToolbarActions;
import com.usik.pulseapp.mvp.view.BaseMvpView;
import com.usik.pulseapp.ui.activity.BaseActivity;
import com.usik.pulseapp.ui.activity.HomeActivity;

import butterknife.ButterKnife;


public abstract class BaseFragment extends MvpAppCompatFragment implements BaseMvpView {

    private FragmentComponent fragmentComponent;
    private Toolbar toolbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        fragmentComponent = getActivityComponent()
                .providesFragmentComponent(new FragmentModule(this));
        injectDependencies();
        super.onCreate(savedInstanceState);

        FragmentArgs.inject(this); // read @Arg fields
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getLayoutRes(), container, false);
        injectViews(view);

        toolbar = getToolbar();
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            showDrawerToggleButton(toolbar);
        }

        initViews();
        return view;
    }

    protected abstract int getLayoutRes();

    public abstract void injectDependencies();

    protected abstract Toolbar getToolbar();

    protected abstract void initViews();

    public void injectViews(View view) {
        ButterKnife.bind(this, view);
    }


    private ActivityComponent getActivityComponent() {
        return ((BaseActivity) getActivity()).getActivityComponent();
    }

    public FragmentComponent getFragmentComponent() {
        return fragmentComponent;
    }

    public void setSupportActionBar(Toolbar toolbar) {
        ((ToolbarActions) getActivity()).setSupportToolBar(toolbar);
    }

    public void showDrawerToggleButton() {
        ((ToolbarActions) getActivity()).showDrawerToggleButton();
    }

    public void showDrawerToggleButton(Toolbar toolbar) {
        ((ToolbarActions) getActivity()).showDrawerToggleButton(toolbar);
    }

    protected void setToolbarTitle(String toolbarTitle) {
        if (toolbar != null) {
            toolbar.setTitle(toolbarTitle);
            toolbar.setSubtitle("");
        }
    }

    protected void setToolbarColor(int color) {
        if (toolbar != null) {
            toolbar.setBackgroundColor(color);
        }
    }

    @Override
    public void onError(Throwable e) {
        ((BaseMvpView) getActivity()).onError(e);
    }

    @Override
    public void showMessage(int title, int message) {
        ((HomeActivity) getActivity()).showMessage(title, message);
    }

    @Override
    public void showMessage(int title) {
        ((HomeActivity) getActivity()).showMessage(title);
    }

    @Override
    public void showToastMessage(int title) {
        ((HomeActivity) getActivity()).showMessage(title);
    }

    protected void setTransparentToolbar(boolean transparent) {
        if (toolbar != null) {
            if (transparent) {
                toolbar.setAlpha(0);
            } else {
                toolbar.setAlpha(1);
            }
        }
    }
}
