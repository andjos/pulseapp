package com.usik.pulseapp.ui.fragment;

import android.content.Intent;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.mvp.presenter.ContactsPresenter;
import com.usik.pulseapp.mvp.view.ContactsView;
import com.usik.pulseapp.ui.activity.MapsActivity;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class ContactsFragment extends BaseFragment implements ContactsView {

    public static final String TAG = "ContactsFragment";
    @InjectPresenter
    ContactsPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_contacts;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_contacts_title));
    }

    @OnClick(R.id.fragment_contacts_map)
    public void onContactsClick() {
        startActivity(new Intent(getActivity(), MapsActivity.class));
    }
}
