package com.usik.pulseapp.ui.fragment;

import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.mvp.presenter.NavigationDrawerPresenter;
import com.usik.pulseapp.mvp.view.NavigationDrawerView;

import butterknife.OnClick;

/**
 * Created by Android 3 on 07.04.2017.
 */

@FragmentWithArgs
public class NavigationDrawerFragment extends BaseFragment
        implements NavigationDrawerView {

    @InjectPresenter
    NavigationDrawerPresenter presenter;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_navigation_drawer;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    public Toolbar getToolbar() {
        return null;
    }

    @Override
    protected void initViews() {

    }

    @OnClick(R.id.navigation_drawer_home_ripple_layout)
    public void onHomeClick() {
        presenter.navigateTo(Screens.MAIN_FRAGMENT_SCREEN);
    }

    @OnClick(R.id.navigation_drawer_results_ripple_layout)
    public void onResultsClick() {
        presenter.navigateTo(Screens.RESULTS_FRAGMENT_SCREEN);
    }

    @OnClick(R.id.navigation_drawer_about_ripple_layout)
    public void onAboutClick() {
        presenter.navigateTo(Screens.ABOUT_FRAGMENT_SCREEN);
    }

    @OnClick(R.id.navigation_drawer_contacts_ripple_layout)
    public void onContactsClick() {
        presenter.navigateTo(Screens.CONTACTS_FRAGMENT_SCREEN);
    }

    @OnClick(R.id.navigation_drawer_analys_ripple_layout)
    public void onAnalysClick() {
        presenter.navigateTo(Screens.ANALYS_FRAGMENT_SCREEN);
    }

    @OnClick(R.id.navigation_drawer_logout_ripple_layout)
    public void onLogout() {
        presenter.logout();
    }
}