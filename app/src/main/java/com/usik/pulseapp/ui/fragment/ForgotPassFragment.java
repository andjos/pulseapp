package com.usik.pulseapp.ui.fragment;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.common.Utils;
import com.usik.pulseapp.mvp.presenter.ForgotPassPresenter;
import com.usik.pulseapp.mvp.view.ForgotPassView;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class ForgotPassFragment extends BaseFragment implements ForgotPassView {
    public static final String TAG = "ForgotPassFragment";
    @InjectPresenter
    ForgotPassPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_forgot_email_layout)
    TextInputLayout emailLayout;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_forgot_pass;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_forgot_title));
    }

    @OnClick(R.id.fragment_forgot_send_button)
    public void onRegisterClick() {
        validate();
    }

    private void validate() {
        String email = emailLayout.getEditText().getText().toString();

        if (!Utils.isValidEmail(email)) {
            emailLayout.setError(getString(R.string.error_email));
            return;
        }
        presenter.forgotPass(email);
    }
}
