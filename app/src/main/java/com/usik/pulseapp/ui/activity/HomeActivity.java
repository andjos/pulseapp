package com.usik.pulseapp.ui.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.usik.pulseapp.App;
import com.usik.pulseapp.R;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.interfaces.ToolbarActions;
import com.usik.pulseapp.mvp.presenter.HomeActivityPresenter;
import com.usik.pulseapp.mvp.view.HomeActivityView;
import com.usik.pulseapp.ui.fragment.NavigationDrawerFragment;
import com.usik.pulseapp.views.SplashView;

import butterknife.BindView;
import timber.log.Timber;

public class HomeActivity extends BaseActivity
        implements HomeActivityView, ToolbarActions, FragmentManager.OnBackStackChangedListener {

    private static final String TAG = HomeActivity.class.getName();

    @InjectPresenter
    HomeActivityPresenter presenter;

    @BindView(R.id.activity_home_fragment_container)
    ViewGroup mainContainer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.fragment_my_orders_toolbar)
    Toolbar toolbar;
    @BindView(R.id.activity_home_latout)
    FrameLayout homeLayout;

    private int backStackCount = 0;
    private ActionBarDrawerToggle drawerToggle;
    SplashView splashView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.switchFragment(Screens.MAIN_FRAGMENT_SCREEN);
        initNavigationDrawer();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.removeNavigator();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setNavigator(baseNavigator);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_home;
    }


    @Override
    public void injectDependency() {
        getActivityComponent().inject(this);
    }

    @Override
    public void showDrawerToggleButton() {
        showDrawerToggleButton(toolbar);
    }

    public void showDrawerToggleButton(Toolbar toolbar) {
        if (toolbar != null) {
            this.toolbar = toolbar;
        }
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
    }


    public void showToggleButton() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
    }

    public void setToolbarColor(int color) {
        if (toolbar != null) {
            toolbar.setBackgroundColor(color);
        }
    }

    @Override
    public Toolbar getToolBar() {
        return toolbar;
    }

    @Override
    public void setSupportToolBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
    }


    @Override
    public void showSplashView() {
        splashView = new SplashView(this);
        splashView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        homeLayout.addView(splashView);


        splashView.setCloseListener(() -> {
            homeLayout.removeView(splashView);
            splashView = null;
            App.isSplashShown = true;
            presenter.newRootScreen(Screens.MAIN_FRAGMENT_SCREEN);
        });
    }

    @Override
    public void onBackPressed() {
        Timber.d("Log_ onBackPressed %s ", backStackCount);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            hideDrawerLayout();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showBackButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            if (backStackCount > 0) {
                onBackPressed();
            } else if (!drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onPerformStartEvent() {

        if (!App.isSplashShown) {//perform first app action
            presenter.showSplashView();
        } else {
            presenter.newRootScreen(Screens.MAIN_FRAGMENT_SCREEN);
        }
    }

    @Override
    public void hideDrawerLayout() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void initNavigationDrawer() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.home_activity_navigation_view,
                new NavigationDrawerFragment(), TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackStackChanged() {
        backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0) {
            showBackButton();
        }
    }
}
