package com.usik.pulseapp.ui.fragment;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.common.Utils;
import com.usik.pulseapp.mvp.presenter.RegisterPresenter;
import com.usik.pulseapp.mvp.view.RegisterView;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class RegisterFragment extends BaseFragment implements RegisterView {
    public static final String TAG = "RegisterFragment";
    @InjectPresenter
    RegisterPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_register_email_layout)
    TextInputLayout emailLayout;
    @BindView(R.id.fragment_register_password_layout)
    TextInputLayout passLayout;
    @BindView(R.id.fragment_register_name_layout)
    TextInputLayout nameLayout;
    @BindView(R.id.fragment_register_surname_layout)
    TextInputLayout surnameLayout;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_register;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_register_title));
    }

    @OnClick(R.id.fragment_register_send_button)
    public void onRegisterClick() {
        validate();
    }

    private void validate() {
        String name = nameLayout.getEditText().getText().toString();
        String surname = surnameLayout.getEditText().getText().toString();
        String email = emailLayout.getEditText().getText().toString();
        String pass = passLayout.getEditText().getText().toString();
        if (name.isEmpty()) {
            nameLayout.setError(getString(R.string.error_name));
            return;
        }
        if (surname.isEmpty()) {
            surnameLayout.setError(getString(R.string.error_surname));
            return;
        }

        if (!Utils.isValidEmail(email)) {
            emailLayout.setError(getString(R.string.error_email));
            return;
        }

        if (pass.isEmpty()) {
            passLayout.setError(getString(R.string.error_pass));
            return;
        }

        presenter.register(name, surname, email, pass);
    }
}
