package com.usik.pulseapp.ui.fragment;

import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.mvp.presenter.AboutPresenter;
import com.usik.pulseapp.mvp.view.AboutView;

import butterknife.BindView;

@FragmentWithArgs
public class AboutFragment extends BaseFragment implements AboutView {
    public static final String TAG = "AboutFragment";
    @InjectPresenter
    AboutPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_about;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_about_title));
    }
}
