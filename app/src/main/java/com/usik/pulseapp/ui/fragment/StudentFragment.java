package com.usik.pulseapp.ui.fragment;

import android.app.Activity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.mvp.presenter.StudentPresenter;
import com.usik.pulseapp.mvp.view.StudentView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android 3 on 07.04.2017.
 */

@FragmentWithArgs
public class StudentFragment extends BaseFragment implements StudentView {

    @InjectPresenter
    StudentPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_students_sp_group)
    Spinner groupSpinner;
    @BindView(R.id.fragment_students_sp_poll)
    Spinner pollSpinner;
    List<String> groupList = new ArrayList<>();
    List<String> pollList = new ArrayList<>();

    String group;
    String poll;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_student;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_student_title));
        initGroupSpinner();
    }

    @OnClick(R.id.fragment_students_send_button)
    public void onClick() {
        //// TODO: 15.04.2017 refactor later
        presenter.send(group, poll);
    }

    private void initGroupSpinner() {
        groupList.add("111");
        groupList.add("231");
        groupList.add("241");
        groupList.add("311");
        groupList.add("431");
        groupList.add("131c");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, groupList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(adapter);

        groupSpinner.setPrompt("Title");
        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

//                sp_group.setPrompt(listGroup.get(selectedItemPosition));
                group = groupList.get(selectedItemPosition);
                initPollSpinner();
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initPollSpinner() {

        pollList.add("Опрос 1");
        pollList.add("Опрос 2");
        pollList.add("Опрос 3");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, pollList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pollSpinner.setAdapter(adapter);

        pollSpinner.setPrompt("Title");
        pollSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

//                sp_group.setPrompt(listGroup.get(selectedItemPosition));
                poll = pollList.get(selectedItemPosition);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
