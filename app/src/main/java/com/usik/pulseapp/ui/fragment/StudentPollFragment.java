package com.usik.pulseapp.ui.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.interfaces.AdapterClickListener;
import com.usik.pulseapp.lists.QuestionsAdapter;
import com.usik.pulseapp.mvp.models.QuestionModel;
import com.usik.pulseapp.mvp.presenter.StudentPollPresenter;
import com.usik.pulseapp.mvp.view.StudentPollView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

/**
 * Created by Andrew on 15.04.2017.
 */

@FragmentWithArgs
public class StudentPollFragment extends BaseFragment implements StudentPollView, AdapterClickListener<QuestionModel> {

    @InjectPresenter
    StudentPollPresenter presenter;
    @Inject
    QuestionsAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_student_poll_recycler)
    RecyclerView recyclerView;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_student_poll;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.student_poll_title));
        adapter.setClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        presenter.sendResult();
    }

    @Override
    public void onItemClick(int position, QuestionModel data) {
        Timber.d("Log_onItem click %s ", data.toString());
    }

    @Override
    public void setData(List<QuestionModel> questionModelList) {
        adapter.setData(questionModelList);
    }
}
