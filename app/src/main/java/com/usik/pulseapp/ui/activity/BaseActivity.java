package com.usik.pulseapp.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.usik.pulseapp.App;
import com.usik.pulseapp.R;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.di.components.ActivityComponent;
import com.usik.pulseapp.di.components.ApplicationComponent;
import com.usik.pulseapp.di.modules.ActivityModule;
import com.usik.pulseapp.mvp.view.BaseMvpView;
import com.usik.pulseapp.navigation.SupportFragmentNavigator;
import com.usik.pulseapp.ui.dialogs.MessageDialog;
import com.usik.pulseapp.ui.fragment.AboutFragmentBuilder;
import com.usik.pulseapp.ui.fragment.AnalysFragment;
import com.usik.pulseapp.ui.fragment.AnalysFragmentBuilder;
import com.usik.pulseapp.ui.fragment.ContactsFragmentBuilder;
import com.usik.pulseapp.ui.fragment.CreatePollFragment;
import com.usik.pulseapp.ui.fragment.CreatePollFragmentBuilder;
import com.usik.pulseapp.ui.fragment.ForgotPassFragmentBuilder;
import com.usik.pulseapp.ui.fragment.HomeFragmentBuilder;
import com.usik.pulseapp.ui.fragment.LoginFragmentBuilder;
import com.usik.pulseapp.ui.fragment.RegisterFragmentBuilder;
import com.usik.pulseapp.ui.fragment.ResultsFragment;
import com.usik.pulseapp.ui.fragment.ResultsFragmentBuilder;
import com.usik.pulseapp.ui.fragment.StudentFragmentBuilder;
import com.usik.pulseapp.ui.fragment.StudentPollFragment;
import com.usik.pulseapp.ui.fragment.StudentPollFragmentBuilder;
import com.usik.pulseapp.ui.fragment.TeacherFragmentBuilder;

import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.commands.Command;

import static com.usik.pulseapp.navigation.AnimationType.FADE_ANIM;
import static com.usik.pulseapp.navigation.AnimationType.NO_ANIM;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseMvpView {

    private ActivityComponent activityComponent;

    protected Navigator baseNavigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.activity_home_fragment_container, FADE_ANIM) {
        @Override
        public void applyCommand(Command command) {
            super.applyCommand(command);
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Screens.MAIN_FRAGMENT_SCREEN:
                    return new HomeFragmentBuilder().build();
                case Screens.STUDENT_FRAGMENT_SCREEN:
                    return new StudentFragmentBuilder().build();
                case Screens.TEACHER_FRAGMENT_SCREEN:
                    return new TeacherFragmentBuilder().build();
                case Screens.LOGIN_FRAGMENT_SCREEN:
                    return new LoginFragmentBuilder().build();
                case Screens.REGISTER_FRAGMENT_SCREEN:
                    return new RegisterFragmentBuilder().build();
                case Screens.FORGOT_PASS_FRAGMENT_SCREEN:
                    return new ForgotPassFragmentBuilder().build();
                case Screens.ABOUT_FRAGMENT_SCREEN:
                    return new AboutFragmentBuilder().build();
                case Screens.CONTACTS_FRAGMENT_SCREEN:
                    return new ContactsFragmentBuilder().build();
                case Screens.CREATE_POLL_FRAGMENT_SCREEN:
                    return new CreatePollFragmentBuilder().build();
                case Screens.ANALYS_FRAGMENT_SCREEN:
                    return new AnalysFragmentBuilder().build();
                case Screens.RESULTS_FRAGMENT_SCREEN:
                    return new ResultsFragmentBuilder().build();
                case Screens.STUDENT_POLL_FRAGMENT_SCREEN:
                    return new StudentPollFragmentBuilder().build();
            }
            return null;
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activityComponent = getApplicationComponent().providesActivityComponent(new ActivityModule(this));
        super.onCreate(savedInstanceState);
        initViews();
    }

    public void showMessage(String title, String message) {
        MessageDialog dialog = new MessageDialog();
        dialog.setCancelable(true);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.show(getSupportFragmentManager(), "message_dialog");
    }

    public void showMessage(String title) {
        MessageDialog dialog = new MessageDialog();
        dialog.setCancelable(true);
        dialog.setTitle(title);
        dialog.setMessage("");
        dialog.show(getSupportFragmentManager(), "message_dialog");
    }


    @Override
    public void showMessage(int title, int message) {
        showMessage(getString(title), getString(message));
    }

    @Override
    public void showMessage(int title) {
        showMessage(getString(title));
    }

    @Override
    public void showToastMessage(int title) {
        Toast.makeText(this, title, Toast.LENGTH_SHORT).show();
    }

    private void initViews() {
        setContentView(getLayoutRes());
        ButterKnife.bind(this);
    }

    @Override
    public void onError(Throwable throwable) {
        MessageDialog dialog = new MessageDialog();
        dialog.setCancelable(true);
        dialog.setTitle(getString(R.string.error));
        dialog.show(getSupportFragmentManager(), "message_dialog");
    }

    ApplicationComponent getApplicationComponent() {
        return App.getApplicationComponent();
    }

    public abstract int getLayoutRes();

    public abstract void injectDependency();

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

}
