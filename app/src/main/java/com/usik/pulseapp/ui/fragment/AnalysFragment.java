package com.usik.pulseapp.ui.fragment;

import android.support.v7.widget.Toolbar;

import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.usik.pulseapp.R;
import com.usik.pulseapp.mvp.view.AnalysView;

import butterknife.BindView;

/**
 * Created by Andrew on 15.04.2017.
 */

@FragmentWithArgs
public class AnalysFragment extends BaseFragment implements AnalysView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_analys_graph)
    GraphView graphView;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_analys;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);

    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.navigation_drawer_menu_analys));

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, 3),
                new DataPoint(1, 8),
                new DataPoint(2, 6),
                new DataPoint(3, 8),
                new DataPoint(4, 10)
        });
        graphView.addSeries(series);
    }
}
