package com.usik.pulseapp.ui.fragment;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.common.Utils;
import com.usik.pulseapp.mvp.presenter.LoginPresenter;
import com.usik.pulseapp.mvp.view.LoginView;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class LoginFragment extends BaseFragment implements LoginView {
    public static final String TAG = "LoginFragment";
    @InjectPresenter
    LoginPresenter presenter;

    @BindView(R.id.fragment_login_email_layout)
    TextInputLayout emailLayout;
    @BindView(R.id.fragment_login_password_layout)
    TextInputLayout passLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_login_title));
    }

    @OnClick(R.id.fragment_login_send_button)
    public void onLoginClick() {
        validate();
    }

    @OnClick(R.id.fragment_login_forgot_pass_text)
    public void onForgotClick() {
        presenter.forgotPass();
    }

    @OnClick(R.id.fragment_login_register_text)
    public void onRegisterCLick() {
        presenter.register();
    }

    private void validate() {
        String email = emailLayout.getEditText().getText().toString();
        String pass = passLayout.getEditText().getText().toString();

        if (!Utils.isValidEmail(email)) {
            emailLayout.setError("Email введено не верно!");
            return;
        }

        if (pass.isEmpty()) {
            passLayout.setError("Введите пароль!");
            return;
        }

        presenter.login(email, pass);
    }
}
