package com.usik.pulseapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.mvp.presenter.HomeFragmentPresenter;
import com.usik.pulseapp.mvp.view.HomeFragmentView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android 3 on 07.04.2017.
 */

@FragmentWithArgs
public class HomeFragment extends BaseFragment implements HomeFragmentView {

    @InjectPresenter
    HomeFragmentPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
    }

    @Override
    public void onStop() {
        super.onStop();
        setTransparentToolbar(false);
        setToolbarColor(R.color.colorPrimary);
    }

    @Override
    public void onStart() {
        super.onStart();
        setTransparentToolbar(true);
        setToolbarColor(R.color.colorWhite);
    }

    @OnClick(R.id.fragment_home_student_layout)
    public void onStudentClick() {
        presenter.onStudentClick();
    }

    @OnClick(R.id.fragment_home_teacher_layout)
    public void onTeacherClick() {
        presenter.onTeacherClick();
    }
}
