package com.usik.pulseapp.ui.fragment;

import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.mvp.presenter.ResultsPresenter;
import com.usik.pulseapp.mvp.view.ResultsView;

import butterknife.BindView;

@FragmentWithArgs
public class ResultsFragment extends BaseFragment implements ResultsView {
    public static final String TAG = "ResultsFragment";
    @InjectPresenter
    ResultsPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_results;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_results_title));
    }
}
