package com.usik.pulseapp.ui.fragment;

import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.usik.pulseapp.R;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.mvp.presenter.TeacherPresenter;
import com.usik.pulseapp.mvp.view.TeacherView;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class TeacherFragment extends BaseFragment implements TeacherView {

    public static final String TAG = "TeacherFragment";

    @InjectPresenter
    TeacherPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_teacher;
    }

    @Override
    public void injectDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initViews() {
        setToolbarTitle(getString(R.string.fragment_teacher_title));
    }

    @OnClick(R.id.fragment_teacher_new_poll_floating_button)
    public void onFloatingClick() {
        presenter.navigateTo(Screens.CREATE_POLL_FRAGMENT_SCREEN);
    }
}
