package com.usik.pulseapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.usik.pulseapp.di.components.ApplicationComponent;
import com.usik.pulseapp.di.components.DaggerApplicationComponent;
import com.usik.pulseapp.di.modules.ApiModule;
import com.usik.pulseapp.di.modules.ApplicationModule;
import com.usik.pulseapp.di.modules.CiceroneModule;
import com.usik.pulseapp.di.modules.DataModule;

import ru.terrakok.cicerone.Cicerone;
import timber.log.Timber;


public class App extends Application {
    public static App INSTANCE;
    private static ApplicationComponent appComponent;
    public static boolean isSplashShown;

    public static ApplicationComponent getApplicationComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        INSTANCE = this;
        super.onCreate();
        appComponent = DaggerApplicationComponent
                .builder()
                .apiModule(new ApiModule())
                .applicationModule(new ApplicationModule(this))
                .ciceroneModule(new CiceroneModule(Cicerone.create()))
                .dataModule(new DataModule())
                .build();

        if (BuildConfig.DEBUG) { //init Timber
            Timber.plant(new Timber.DebugTree());
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
