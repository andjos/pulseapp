package com.usik.pulseapp.mvp.view;



public interface HomeActivityView extends BaseMvpView {

    void onPerformStartEvent();

    void showSplashView();

    void hideDrawerLayout();
}
