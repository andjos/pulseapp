package com.usik.pulseapp.mvp.view;

import com.arellomobile.mvp.MvpView;


public interface BaseMvpView extends MvpView {
    void onError(Throwable e);

    void showMessage(int title, int message);

    void showMessage(int title);

    void showToastMessage(int title);


}
