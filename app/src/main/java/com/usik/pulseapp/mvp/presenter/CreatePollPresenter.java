package com.usik.pulseapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.mvp.view.CreatePollView;

/**
 * Created by Andrew on 12.04.2017.
 */

@InjectViewState
public class CreatePollPresenter extends BasePresenter<CreatePollView> {

    public CreatePollPresenter() {
        App.getApplicationComponent().inject(this);
    }
}
