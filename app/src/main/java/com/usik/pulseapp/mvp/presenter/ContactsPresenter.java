package com.usik.pulseapp.mvp.presenter;

import com.usik.pulseapp.App;
import com.usik.pulseapp.mvp.view.ContactsView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class ContactsPresenter extends BasePresenter<ContactsView> {

    public ContactsPresenter() {
        App.getApplicationComponent().inject(this);
    }
}
