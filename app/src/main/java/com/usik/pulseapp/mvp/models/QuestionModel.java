package com.usik.pulseapp.mvp.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Andrew on 15.04.2017.
 */

@Data
@AllArgsConstructor
public class QuestionModel implements Serializable {
    private String pollName;
    private double mark;
}
