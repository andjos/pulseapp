package com.usik.pulseapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.mvp.view.AnalysView;

/**
 * Created by Andrew on 15.04.2017.
 */

@InjectViewState
public class AnalysPresenter extends BasePresenter<AnalysView> {

    public AnalysPresenter() {
        App.getApplicationComponent().inject(this
        );
    }
}
