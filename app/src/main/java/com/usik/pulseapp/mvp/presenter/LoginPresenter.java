package com.usik.pulseapp.mvp.presenter;

import com.usik.pulseapp.App;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.managers.PreferenceManager;
import com.usik.pulseapp.mvp.view.LoginView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {

    @Inject
    Router router;
    @Inject
    PreferenceManager preferenceManager;

    public LoginPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void login(String login, String pass) {

        router.newRootScreen(Screens.TEACHER_FRAGMENT_SCREEN);
        preferenceManager.login();
    }

    public void forgotPass() {
        router.navigateTo(Screens.FORGOT_PASS_FRAGMENT_SCREEN);
    }

    public void register() {
        router.navigateTo(Screens.REGISTER_FRAGMENT_SCREEN);
    }
}
