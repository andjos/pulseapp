package com.usik.pulseapp.mvp.view;

import com.usik.pulseapp.mvp.models.QuestionModel;

import java.util.List;

/**
 * Created by Andrew on 15.04.2017.
 */

public interface StudentPollView extends BaseMvpView {
    void setData(List<QuestionModel> model);
}
