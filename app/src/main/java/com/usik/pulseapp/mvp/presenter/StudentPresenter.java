package com.usik.pulseapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.mvp.models.QuestionModel;
import com.usik.pulseapp.mvp.view.StudentView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

/**
 * Created by Android 3 on 07.04.2017.
 */

@InjectViewState
public class StudentPresenter extends BasePresenter<StudentView> {

    @Inject
    Router router;
    List<QuestionModel> list = new ArrayList<>();

    public StudentPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void send(String group, String pollName) {
        router.navigateTo(Screens.STUDENT_POLL_FRAGMENT_SCREEN);
    }

}
