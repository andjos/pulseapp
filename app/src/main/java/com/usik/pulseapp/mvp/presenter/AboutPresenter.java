package com.usik.pulseapp.mvp.presenter;

import com.usik.pulseapp.App;
import com.usik.pulseapp.mvp.view.AboutView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class AboutPresenter extends BasePresenter<AboutView> {

    public AboutPresenter() {
        App.getApplicationComponent().inject(this);
    }
}
