package com.usik.pulseapp.mvp.presenter;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.di.qualifiers.ApplicationContext;
import com.usik.pulseapp.events.NavigateToNavEvent;
import com.usik.pulseapp.events.NewRootScreenNavEvent;
import com.usik.pulseapp.events.ReplaceNavEvent;
import com.usik.pulseapp.events.SwitchFragmentEvent;
import com.usik.pulseapp.mvp.view.HomeActivityView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;


@InjectViewState
public class HomeActivityPresenter extends BasePresenter<HomeActivityView> {

    @Inject
    Router router;
    @Inject
    NavigatorHolder navigatorHolder;
    @Inject
    @ApplicationContext
    Context context;
    @Inject
    EventBus bus;

    public HomeActivityPresenter() {
        App.getApplicationComponent().inject(this);
    }

    //region overrides
    @Override
    public void attachView(HomeActivityView view) {
        super.attachView(view);
        registerBus();
    }

    @Override
    public void detachView(HomeActivityView view) {
        unregisterBus();
        super.detachView(view);
    }
    //endregion


    //region subscribe
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSwitchFragmentEvent(SwitchFragmentEvent event) {
        if (event.getData() == null) {
            switchFragment(event.getScreen());
        } else {
            switchFragment(event.getScreen(), event.getData());
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNavigateTo(NavigateToNavEvent event) {
        Timber.d("onNavigateTo " + event.getScreen());
        if (event.getData() == null) {
            navigateToFragment(event.getScreen());
        } else {
            navigateToFragment(event.getScreen(), event.getData());
        }
        getViewState().hideDrawerLayout();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReplace(ReplaceNavEvent event) {
        if (event.getData() == null) {
            replaceFragment(event.getScreen());
        } else {
            replaceFragment(event.getScreen(), event.getData());
        }
        getViewState().hideDrawerLayout();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNewRootScreen(NewRootScreenNavEvent event) {
        if (event.getData() == null) {
            newRootScreen(event.getScreen());
        } else {
            newRootScreen(event.getScreen(), event.getData());
        }
    }
    //endregion

    //region methods
    private void registerBus() {
        if (!bus.isRegistered(this)) {
            bus.register(this);
        }
    }

    private void unregisterBus() {
        if (bus.isRegistered(this)) {
            bus.unregister(this);
        }
    }

    public void navigateToFragment(String screen, Object data) {
        router.navigateTo(screen, data);

    }

    public void navigateToFragment(String screen) {
        router.navigateTo(screen);

    }

    public void replaceFragment(String screen, Object data) {
        router.replaceScreen(screen, data);
    }

    public void replaceFragment(String screen) {
        router.replaceScreen(screen);
    }

    public void newRootScreen(String screen, Object data) {
        router.newRootScreen(screen, data);
    }

    public void newRootScreen(String screen) {
        router.newRootScreen(screen);

    }

    public void backToScreen(String screen) {
        router.backTo(screen);

    }
    public void showSplashView() {
        getViewState().showSplashView();
    }

    public void setNavigator(Navigator navigator) {
        navigatorHolder.setNavigator(navigator);
    }

    public void removeNavigator() {
        navigatorHolder.removeNavigator();
    }

    public void switchFragment(String screen, Object data) {
        router.navigateTo(screen, "data");
    }

    public void switchFragment(String screen) {
        router.navigateTo(screen);
    }

    @Override
    protected void onFirstViewAttach() {
        getViewState().onPerformStartEvent();
        super.onFirstViewAttach();
    }

    //endregion
}
