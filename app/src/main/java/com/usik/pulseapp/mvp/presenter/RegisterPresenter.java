package com.usik.pulseapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.usik.pulseapp.App;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.managers.PreferenceManager;
import com.usik.pulseapp.mvp.view.RegisterView;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class RegisterPresenter extends BasePresenter<RegisterView> {

    @Inject
    Router router;
    @Inject
    PreferenceManager preferenceManager;


    public RegisterPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void register(String name, String suremane, String email, String pass) {

        router.newRootScreen(Screens.TEACHER_FRAGMENT_SCREEN);
        preferenceManager.login();
    }
}
