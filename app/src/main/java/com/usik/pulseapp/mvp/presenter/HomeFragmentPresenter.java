package com.usik.pulseapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.managers.PreferenceManager;
import com.usik.pulseapp.mvp.view.HomeFragmentView;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

/**
 * Created by Android 3 on 07.04.2017.
 */

@InjectViewState
public class HomeFragmentPresenter extends BasePresenter<HomeFragmentView> {

    @Inject
    Router router;
    @Inject
    PreferenceManager preferenceManager;

    public HomeFragmentPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void onStudentClick() {
        router.navigateTo(Screens.STUDENT_FRAGMENT_SCREEN);
    }

    public void onTeacherClick() {
        if (preferenceManager.isLogin()) {
            router.navigateTo(Screens.TEACHER_FRAGMENT_SCREEN);
        } else {
            router.navigateTo(Screens.LOGIN_FRAGMENT_SCREEN);
        }
    }
}
