package com.usik.pulseapp.mvp.presenter;

import com.usik.pulseapp.App;
import com.usik.pulseapp.mvp.view.ResultsView;
import com.arellomobile.mvp.InjectViewState;

@InjectViewState
public class ResultsPresenter extends BasePresenter<ResultsView> {

    public ResultsPresenter() {
        App.getApplicationComponent().inject(this);
    }
}
