package com.usik.pulseapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.mvp.models.QuestionModel;
import com.usik.pulseapp.mvp.view.StudentPollView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 15.04.2017.
 */

@InjectViewState
public class StudentPollPresenter extends BasePresenter<StudentPollView> {

    List<QuestionModel> list = new ArrayList<>();
    public StudentPollPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void sendResult() {
        list.add(new QuestionModel("Устраивает ли вас темп занятия?", 1));
        list.add(new QuestionModel("Понятен ли материал?", 1));
        list.add(new QuestionModel("Успеваете конспектировать?", 1));
        getViewState().setData(list);
    }
}
