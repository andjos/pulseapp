package com.usik.pulseapp.mvp.presenter;


import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.mvp.view.TeacherView;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class TeacherPresenter extends BasePresenter<TeacherView> {

    @Inject
    Router router;

    public TeacherPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void navigateTo(String screen) {
        router.navigateTo(screen);
    }
}
