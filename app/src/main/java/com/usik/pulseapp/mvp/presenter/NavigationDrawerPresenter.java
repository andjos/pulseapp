package com.usik.pulseapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.usik.pulseapp.App;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.events.ReplaceNavEvent;
import com.usik.pulseapp.managers.PreferenceManager;
import com.usik.pulseapp.mvp.view.NavigationDrawerView;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;


@InjectViewState
public class NavigationDrawerPresenter extends BasePresenter<NavigationDrawerView> {


    @Inject
    EventBus bus;
    @Inject
    PreferenceManager preferenceManager;

    public NavigationDrawerPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void navigateTo(String screen) {
        bus.post(new ReplaceNavEvent(screen));
    }

    public void logout() {
        preferenceManager.logout();
        navigateTo(Screens.MAIN_FRAGMENT_SCREEN);
    }
}
