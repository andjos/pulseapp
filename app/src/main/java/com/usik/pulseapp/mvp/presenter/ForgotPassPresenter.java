package com.usik.pulseapp.mvp.presenter;

import com.usik.pulseapp.App;
import com.usik.pulseapp.R;
import com.usik.pulseapp.Screens;
import com.usik.pulseapp.mvp.view.ForgotPassView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class ForgotPassPresenter extends BasePresenter<ForgotPassView> {

    @Inject
    Router router;

    public ForgotPassPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void forgotPass(String email) {
        getViewState().showToastMessage(R.string.forgot_pass_message);
        router.newRootScreen(Screens.LOGIN_FRAGMENT_SCREEN);
    }
}
