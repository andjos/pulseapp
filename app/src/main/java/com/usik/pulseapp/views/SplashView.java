package com.usik.pulseapp.views;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;

import com.usik.pulseapp.R;
import com.usik.pulseapp.common.Constants;

public class SplashView extends FrameLayout {
    private OnSplashCloseListener closeListener;

    public SplashView(Context context) {
        super(context);
        init(context);
    }

    public SplashView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_splash, this);
        animate()
                .setDuration(300)
                .alpha(0.f)
                .setInterpolator(new AccelerateInterpolator())
                .setStartDelay(Constants.SPLASH_DURATION)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (closeListener != null) {
                            closeListener.onClose();
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }


    public void setCloseListener(OnSplashCloseListener closeListener) {
        this.closeListener = closeListener;
    }

    public interface OnSplashCloseListener {
        void onClose();
    }
}
