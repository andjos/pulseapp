package com.usik.pulseapp.di.qualifiers;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
