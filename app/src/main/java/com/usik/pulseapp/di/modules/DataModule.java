package com.usik.pulseapp.di.modules;

import android.app.Application;
import android.content.Context;

import com.usik.pulseapp.data.DatabaseManager;
import com.usik.pulseapp.di.qualifiers.ApplicationContext;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;


@Module
public class DataModule {

    @Provides
    Realm provideRealm(Application application) {
        Realm.init(application);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        return Realm.getInstance(config);
    }

    @Provides
    DatabaseManager provideDatabaseManager(Realm realm) {
        return new DatabaseManager(realm);
    }

}
