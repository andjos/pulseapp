package com.usik.pulseapp.di.components;


import com.usik.pulseapp.di.modules.FragmentModule;
import com.usik.pulseapp.di.scopes.FragmentScope;
import com.usik.pulseapp.ui.fragment.AboutFragment;
import com.usik.pulseapp.ui.fragment.AnalysFragment;
import com.usik.pulseapp.ui.fragment.ContactsFragment;
import com.usik.pulseapp.ui.fragment.CreatePollFragment;
import com.usik.pulseapp.ui.fragment.ForgotPassFragment;
import com.usik.pulseapp.ui.fragment.HomeFragment;
import com.usik.pulseapp.ui.fragment.LoginFragment;
import com.usik.pulseapp.ui.fragment.NavigationDrawerFragment;
import com.usik.pulseapp.ui.fragment.RegisterFragment;
import com.usik.pulseapp.ui.fragment.ResultsFragment;
import com.usik.pulseapp.ui.fragment.StudentFragment;
import com.usik.pulseapp.ui.fragment.StudentPollFragment;
import com.usik.pulseapp.ui.fragment.TeacherFragment;

import dagger.Subcomponent;

@FragmentScope
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

    void inject(HomeFragment homeFragment);

    void inject(NavigationDrawerFragment navigationDrawerFragment);

    void inject(StudentFragment studentFragment);

    void inject(TeacherFragment teacherFragment);

    void inject(ResultsFragment resultsFragment);

    void inject(AboutFragment aboutFragment);

    void inject(LoginFragment loginFragment);

    void inject(RegisterFragment registerFragment);

    void inject(ForgotPassFragment forgotPassFragment);

    void inject(CreatePollFragment createPollFragment);

    void inject(AnalysFragment analysFragment);

    void inject(StudentPollFragment studentPollFragment);

    void inject(ContactsFragment contactsFragment);
}
