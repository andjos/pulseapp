package com.usik.pulseapp.di.scopes;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}
