package com.usik.pulseapp.di.components;

import com.usik.pulseapp.ui.activity.HomeActivity;
import com.usik.pulseapp.di.modules.ActivityModule;
import com.usik.pulseapp.di.modules.FragmentModule;
import com.usik.pulseapp.di.scopes.ActivityScope;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {
    FragmentComponent providesFragmentComponent(FragmentModule fragmentModule);

    void inject(HomeActivity homeActivity);
}
