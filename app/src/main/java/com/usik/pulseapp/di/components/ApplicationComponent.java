package com.usik.pulseapp.di.components;

import com.usik.pulseapp.di.modules.ActivityModule;
import com.usik.pulseapp.di.modules.ApplicationModule;
import com.usik.pulseapp.di.scopes.ApplicationScope;
import com.usik.pulseapp.mvp.presenter.AboutPresenter;
import com.usik.pulseapp.mvp.presenter.AnalysPresenter;
import com.usik.pulseapp.mvp.presenter.ContactsPresenter;
import com.usik.pulseapp.mvp.presenter.CreatePollPresenter;
import com.usik.pulseapp.mvp.presenter.ForgotPassPresenter;
import com.usik.pulseapp.mvp.presenter.HomeActivityPresenter;
import com.usik.pulseapp.mvp.presenter.HomeFragmentPresenter;
import com.usik.pulseapp.mvp.presenter.LoginPresenter;
import com.usik.pulseapp.mvp.presenter.NavigationDrawerPresenter;
import com.usik.pulseapp.mvp.presenter.RegisterPresenter;
import com.usik.pulseapp.mvp.presenter.ResultsPresenter;
import com.usik.pulseapp.mvp.presenter.StudentPollPresenter;
import com.usik.pulseapp.mvp.presenter.StudentPresenter;
import com.usik.pulseapp.mvp.presenter.TeacherPresenter;

import dagger.Component;

@ApplicationScope
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    ActivityComponent providesActivityComponent(ActivityModule activityModule);

    void inject(HomeActivityPresenter homeActivityPresenter);

    void inject(NavigationDrawerPresenter navigationDrawerPresenter);

    void inject(StudentPresenter studentPresenter);

    void inject(HomeFragmentPresenter homeFragmentPresenter);

    void inject(ResultsPresenter resultsPresenter);

    void inject(ContactsPresenter contactsPresenter);

    void inject(TeacherPresenter teacherPresenter);

    void inject(AboutPresenter aboutPresenter);

    void inject(LoginPresenter loginPresenter);

    void inject(RegisterPresenter registerPresenter);

    void inject(ForgotPassPresenter forgotPassPresenter);

    void inject(CreatePollPresenter createPollPresenter);

    void inject(AnalysPresenter analysPresenter);

    void inject(StudentPollPresenter studentPollPresenter);
}
