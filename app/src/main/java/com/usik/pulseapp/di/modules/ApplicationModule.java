package com.usik.pulseapp.di.modules;

import android.app.Application;
import android.content.Context;


import com.usik.pulseapp.di.qualifiers.ApplicationContext;
import com.usik.pulseapp.di.scopes.ApplicationScope;

import org.greenrobot.eventbus.EventBus;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class, CiceroneModule.class, DataModule.class})
public class ApplicationModule {
    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationScope
    EventBus provideEventBus() {
        return EventBus.builder().throwSubscriberException(false).eventInheritance(true).build();
    }

    @Provides
    @ApplicationContext
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }
}